# AICONCLASS

A test dataset and challenge to apply machine learning to collections described with the [Iconclass](https://iconclass.org/) classification system.

**Please contact us with comments or suggestions, or if you do something intresting with this dataset so we can share it with the community**

Mail me at posthumus@brill.com or connect on Twitter: [@epoz](https://twitter.com/epoz)

## Data Description

A dataset of 87749 images with assigned Iconclass notations to be used in training.
[The dataset is available as a 3.1GB zip file.](https://labs.brill.com/ictestset/data.zip) (MD5: f20c7fc2d87011558bb4e918dba3b5f1) All image files are in a single directory, so beware of just unzipping the file.

The _data.json_ file is a map of filenames to Iconclass notations, here is what the first few entries look like:

```
{
  "IIHIM_1956438510.jpg": [
    "31A235",
    "31A24(+1)",
    "61B(+54)",
    "61B:31A2212(+1)",
    "61B:31D14"
  ],
  "IIHIM_-859728949.jpg": [
    "41D92",
    "25G41"
  ],
  "IIHIM_1207680098.jpg": [
    "11H",
    "11I35",
    "11I36"
  ],
  "IIHIM_-743518586.jpg": [
    "11F25",
    "11FF25",
    "41E2"
  ]
}
```

## Image files

The image files in this dataset have maximum size of 500 pixels to the longest side, and have been sampled randomly from the [Arkyves](https://www.arkyves.org) database.

## Using Iconclass from Python

To perform analysis and extract futher meaning from the assigned classifications you can use the Iconclass Python package. It can be installed with:

`pip install iconclass`

Then in a Python interpreter you can use Python like:

```
>>> import iconclass
>>> iconclass.get('0')
```

**NOTE:** The first time you dop a iconclass.get(...) class, the complete Iconclass system will be downloaded as a sqlite file to the current working directory. This is a circa 50MB file, and it also contains a wealth of useful data for analysis purposes.

To get structural information on the classification codes for an image, you can for example say:

```
>>> iconclass.get('25G41')

{'c': ['25G41(...)',
       '25G411',
       '25G412',
       '25GG41',
       '25G41(+0)',
       '25G41(+1)',
       '25G41(+2)',
       '25G41(+3)'],
 'kw': {'de': ['Blume'],
        'en': ['flower'],
        'es': ['flor'],
        'fi': ['kukka'],
        'fr': ['fleur'],
        'it': ['fiore'],
        'nl': ['bloem'],
        'pt': ['flor']},
 'n': '25G41',
 'p': ['2', '25', '25G', '25G4', '25G41'],
 'txt': {'de': 'Blumen',
         'en': 'flowers',
         'fi': 'kukat',
         'fr': 'fleurs',
         'it': 'fiori',
         'nl': 'Bloemen….(met NAAM)',
         'pt': 'flores',
         'zh': '花卉'}}

```

The keys in the returned dict are:

- 'n' The notation of this node (the same thing as the first parameter of the .get() call)
- 'p' Path to this node, starting from the root of the hierarchy
- 'c' Children (descendants) of this node
- 'kw' Keywords, a dict keyed on two-letter language code containing keywords asociated with this entry
- 'txt Textual descriptions of this notation, a dict of strings keyed on two-letter language code.

You can also retrieve the notations in a list like:

```
 iconclass.get_list(["11F25", "41E2"])
```
